import React from 'react'
import { Stack, Box } from '@chakra-ui/react'
import TableComponent from './Table'

const StackComponent = () => {
  return (
    <Stack>
      <Box shadow='lg' borderWidth='1px'>
        <TableComponent />
      </Box>
    </Stack>
  )
}

export default StackComponent
