import React from 'react'
import { Divider, Center, Image, Box, Button } from '@chakra-ui/react'
import { EmailIcon } from '@chakra-ui/icons'

function SidebarComponent() {
  return (
    <>
    <Center className='logo'>
        <Image src='logo.png'/>
    </Center>
    <Divider />
    <Box>
        <Button variant='ghost'
            colorScheme='blackAlpha'
            size='lg'
            leftIcon={<EmailIcon boxSize={6} />}
            width='100%'    
        >
            Kafka Consumers
        </Button>
        {/* <Button variant='ghost'
            colorScheme='blackAlpha'
            size='lg'
            leftIcon={<SpinnerIcon boxSize={6} />}
            width='100%'
        >
            Auto Actions
        </Button>       */}
    </Box>
    </>
  )
}

export default SidebarComponent