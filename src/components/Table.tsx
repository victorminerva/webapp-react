import React, { Component } from 'react'
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
  Switch 
} from '@chakra-ui/react'

  
export default class TableComponent extends Component<{}> {
  
  constructor(props) {
    super(props);
    this.state = {consumers: []};
  }
  
  componentDidMount() {
    fetch('/KafkaConsumer/Registry')
        .then(response => response.json())
        .then(data => this.setState({consumers: data}));
  }
  
  render() {
    const { consumers } = this.state;
    const consumersList = consumers.map(consumer => {
      return <Tr>
              <Td>{consumer.consumerId}</Td>
              <Td>{consumer.assignments.length}</Td>
              <Td>{String(consumer.active)}</Td>
              <Td><Switch isChecked={consumer.active} colorScheme='teal' /></Td>
            </Tr>
    })

    return (
      <TableContainer>
        <Table variant='striped'>
            <TableCaption>List of Auto Action Consumers</TableCaption>
            <Thead>
            <Tr>
                <Th>Consumer Id</Th>
                <Th>Members</Th>
                <Th>Actived</Th>
                <Th></Th>
            </Tr>
            </Thead>
              <Tbody>
              {consumersList}
            </Tbody>
            <Tfoot>
            <Tr>
                <Th>Consumer Id</Th>
                <Th>Members</Th>
                <Th>Actived</Th>
                <Td></Td>
            </Tr>
            </Tfoot>
        </Table>
      </TableContainer>
    );
  }
}