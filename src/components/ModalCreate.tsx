import React from 'react'
import {
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    FormControl,
    FormLabel,
    Input,
    Button
} from '@chakra-ui/react'
  
const onSave = () => {
    alert("SAVED")
}

function ModalCreateComponent() {
    const { isOpen, onClose } = useDisclosure()
    const initialRef = React.useRef(null)
    const finalRef = React.useRef(null)

    return (
        <Modal
            initialFocusRef={initialRef}
            finalFocusRef={finalRef}
            isOpen={isOpen}
            onClose={onClose}
            >
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Create a Consumer Group</ModalHeader>
                <ModalCloseButton />
                <ModalBody pb={6}>
                    <FormControl>
                        <FormLabel>Tenant</FormLabel>
                        <Input ref={initialRef} placeholder='Tenant' />
                    </FormControl>

                    <FormControl mt={4}>
                        <FormLabel>Members</FormLabel>
                        <Input placeholder='Members' />
                    </FormControl>
                    
                    <FormControl mt={4}>
                        <FormLabel>Start Immediately</FormLabel>
                        <Input placeholder='Start Immediately' />
                    </FormControl>
                </ModalBody>

                <ModalFooter>
                    <Button onClick={onSave} colorScheme='blue' mr={3}>Save</Button>
                    <Button onClick={onClose}>Cancel</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}

export default ModalCreateComponent