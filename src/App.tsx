import React, { Component } from 'react'
import './App.css'
import PageRoutes from './pages/Routes'
import { GlobalStyles } from './styles/globalStyles'

export default class App extends Component {
  render() {
    return (
      <>
        <GlobalStyles />
        <PageRoutes />
      </>
    );
  }
}
