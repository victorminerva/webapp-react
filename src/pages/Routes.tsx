import React, { Component } from 'react'
import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
import AdminPage from "./admin";

class PageRoutes extends Component{
  render() {    
    return (
      <BrowserRouter>
          <Routes>
              <Route path="/" element={<AdminPage/>} />
          </Routes>
      </BrowserRouter>
    )
  }
}

export default PageRoutes