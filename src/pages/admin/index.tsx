import React, { Component } from 'react'
import { Input, InputGroup, InputRightElement } from '@chakra-ui/react'
import { SearchIcon } from '@chakra-ui/icons'
import StackComponent from '../../components/Stack';
import { Button } from '@chakra-ui/react'
import Sidebar from '../../components/Sidebar';

export default class AdminPage extends Component {  
  openModalCreate() {
    alert("OPEN A MODAL")
  }

  render() {
    return (
      <>
        <div className='container'>
          <div className="fixed">
            <Sidebar />
          </div>
          <div className="flex-item">
            <div className='flex-header'>
              <InputGroup className='inputField'>
                <Input placeholder='Tenant'
                  _placeholder={{ opacity: 1, color: 'white' }}
                  size='lg' />
                <InputRightElement children={<SearchIcon boxSize={5} color='gray.200' />} />
              </InputGroup>
              <Button className='buttonField' colorScheme='blackAlpha'
                onClick={this.openModalCreate}
                size='md'
                height='48px'
                width='200px'
              >
                Create
              </Button>
            </div>
            <div className="flex-item page">
              <StackComponent />
            </div>
          </div>
        </div>
      </>
    );  
  }
  
}