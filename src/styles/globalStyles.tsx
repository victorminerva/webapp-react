import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
body {
  margin: 0;
  padding: 0;
  background: #f7f7f7;
  font-family: 'Lato', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

code {
  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
}

.container{
    display: flex;
}

.fixed{
    width: 15%;
    min-height: 100ch;
}

.flex-item{
    flex-grow: 1;
    background-color: #e4e4e4;
}

.flex-header {
  display: flex;
  width: 100%;
  height: 10vw;
  background-color: #22aafe;
}

.inputField {
  position: fixed;
  width: 300px;
  margin: 20px;
}

.buttonField {
  position: fixed;
  right: 0px;
  margin: 20px;
}

.page {
  margin: 20px;
  background-color: white;
}

.logo {
  margin-top: 50px;
  margin-bottom: 50px;
  max-width: 100%;
  height: auto;
}
`